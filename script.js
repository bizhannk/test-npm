function createCounter(startValue = 0) {
  let number = startValue + 1;

  return function () {
    return number++;
  };
}
const counter = createCounter(44);
console.log(counter());
console.log(counter());

function multiply(x) {
  return function (y) {
    return function (z) {
      return x * y * z;
    };
  };
}

console.log(multiply(2)(4)(6)); // 48
console.log(multiply(3)(3)(3)); // 27

class Logger {
  constructor() {
    this.logs = [];
  }
  log(newlog) {
    return this.logs.push(newlog);
  }
  getLog() {
    return this.logs;
  }
  clearLog() {
    return (this.logs = []);
  }
}

const logger = new Logger();

logger.log('Event 1');
logger.log('Event 2');
logger.log('Event 3');

logger.getLog();
console.log(logger.getLog());
logger.clearLog();
console.log(logger.clearLog());

Array.prototype.shuffle = function () {
  var currentIndex = this.length,
    temporaryValue,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = this[currentIndex];
    this[currentIndex] = this[randomIndex];
    this[randomIndex] = temporaryValue;
  }

  return this;
};

console.log([1, 2, 3, 4].shuffle()); // result: some random shuffling ex: [2,3,4,1]
console.log(['a', 'b', 'c'].shuffle()); // result: some random shuffling ex: ['c', 'b', 'a']

const obj = {
  hello: 'hello',
};
console.log(obj['hello']);

